// import React from "react";
// import Display from "./Display";
// import ButtonPanel from "./ButtonPanel";
// import calculate from "../logic/calculate";
// import LineageDag from 'react-lineage-dag';
// import 'react-lineage-dag/dist/index.css';
// // import LineageTable  from 'react-lineage-dag'


// const data = {
//   tables: [
//     {
//       id: '1',
//       name: 'table-1',
//       isCollapse: true,
//       fields: [
//         {
//           name: 'id',
//           title: 'id',
//           primary_key: true
//         },
//         {
//           name: 'age',
//           title: 'age'
//         }
//       ]
//     },
//     {
//       id: '2',
//       name: 'table-2',
//       isCollapse: false,
//       fields: [
//         {
//           name: 'id',
//           title: 'id',
//         },
//         {
//           name: 'age',
//           title: 'age'
//         },
//         {
//           name: 'id2',
//           title: 'id2',
//         },
//         {
//           name: 'age2',
//           title: 'age2'
//         },
//         {
//           name: 'id3',
//           title: 'id3',
//         },
//         {
//           name: 'age3',
//           title: 'age3'
//         },
//         {
//           name: 'id4',
//           title: 'id4',
//         },
//         {
//           name: 'age4',
//           title: 'age4'
//         }
//       ]
//     },
//     {
//       id: '3',
//       name: 'table-3',
//       isCollapse: true,
//       fields: [
//         {
//           name: 'id',
//           title: 'id',
//           primary_key: true
//         },
//         {
//           name: 'age',
//           title: 'age'
//         }
//       ]      
//     },
//     {
//       id: '4',
//       name: 'table-4',
//       isCollapse: true,
//       fields: [
//         {
//           name: 'id',
//           title: 'id',
//         },
//         {
//           name: 'age',
//           title: 'age'
//         }
//       ]      
//     }
//   ],
//   columns: [
//     {
//       key: 'id',
//       primary_key: true
//     }
//   ],
//   relations: [
//     {
//       srcTableId: '1',
//       tgtTableId: '2',
//       srcTableColName: 'id',
//       tgtTableColName: 'age'
//     },
//     {
//       srcTableId: '1',
//       tgtTableId: '2',
//       srcTableColName: 'id',
//       tgtTableColName: 'id'
//     },    
//     {
//       srcTableId: '1',
//       tgtTableId: '3',
//       srcTableColName: 'id',
//       tgtTableColName: 'age'      
//     },
//     {
//       srcTableId: '2',
//       tgtTableId: '4',
//       srcTableColName: 'id',
//       tgtTableColName: 'age'      
//     },
//     {
//       srcTableId: '2',
//       tgtTableId: '4',
//       srcTableColName: 'id',
//       tgtTableColName: 'id'
//     },
//     {
//       srcTableId: '2',
//       tgtTableId: '4'
//     }    
//   ]
// }

// const App = () => {
//   return (
//     <LineageDag {...data} />
//   )
// }
// export default App


import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import {Layout, Tooltip} from 'antd';
import * as _ from 'lodash';
import LineageDag from 'react-lineage-dag';

import 'antd/dist/antd.css';
import 'react-lineage-dag/dist/index.css';
import './index.less';
import '../index.css';


import { BorderOuterOutlined, DownSquareOutlined, CloseCircleOutlined, StarOutlined } from '@ant-design/icons';

const {Header} = Layout;

export let mockData = {
  height: 800,
  tables: [
    {
      id: '1',
      name: 'table-1',
      // isCollapse: true,
      fields: [
        {
          name: 'id',
          title: 'id',
        },
        {
          name: 'age',
          title: 'age'
        }
      ]
    },
    {
      id: '2',
      name: 'table-2',
      // isCollapse: true,
      fields: [
        {
          name: 'id',
          title: 'id',
        },
        {
          name: 'age',
          title: 'age'
        },
        {
          name: 'id2',
          title: 'id2',
        },
        {
          name: 'age2',
          title: 'age2'
        },
        {
          name: 'id3',
          title: 'id3',
        },
        {
          name: 'age3',
          title: 'age3'
        },
        {
          name: 'id4',
          title: 'id4',
        },
        {
          name: 'age4',
          title: 'age4'
        }
      ]
    },
    {
      id: '3',
      name: 'table-3',
      isCollapse: true,
      fields: [
        {
          name: 'id',
          title: 'id',
        },
        {
          name: 'age',
          title: 'age'
        }
      ]      
    },
    {
      id: '4',
      name: 'table-4',
      isCollapse: true,
      fields: [
        {
          name: 'id',
          title: 'id',
        },
        {
          name: 'age',
          title: 'age'
        }
      ]      
    }
  ],
  relations: [
    {
      srcTableId: '1',
      tgtTableId: '2',
      srcTableColName: 'id',
      tgtTableColName: 'age'
    },
    {
      srcTableId: '1',
      tgtTableId: '2',
      srcTableColName: 'id',
      tgtTableColName: 'id'
    },    
    {
      srcTableId: '1',
      tgtTableId: '3',
      srcTableColName: 'id',
      tgtTableColName: 'age'      
    },
    {
      srcTableId: '2',
      tgtTableId: '4',
      srcTableColName: 'id',
      tgtTableColName: 'age'      
    },
    {
      srcTableId: '2',
      tgtTableId: '4',
      srcTableColName: 'id',
      tgtTableColName: 'id'
    },
    {
      srcTableId: '2',
      tgtTableId: '4'
    }    
  ]
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    const {tables, relations} = mockData;
    this.state = {
      tables,
      relations,
      canvas: null,
      actionMenu: [{
        icon: <StarOutlined />,
        key: 'star',
        onClick: () => {
          alert('点击收藏！')
        }
      }]
    };
    this.columns = [{
      key: 'name',
      primaryKey: true
    }, {
      key: 'title',
    }];
    this.operator = [{
      id: 'isExpand',
      name: 'Expend',
      icon: <Tooltip title='Expend Or Collapse'><BorderOuterOutlined /></Tooltip>,
      onClick: (nodeData) => {
        // 展开血缘
        let tables = _.cloneDeep(this.state.tables);
        let table = _.find(tables, (item) => item.id === nodeData.id);
        table.isCollapse = !!!table.isCollapse;
        this.setState({
          tables,
          centerId: table.id
        });
      }
    }, {
      id: 'explore',
      name: 'Explore',
      icon: <Tooltip title='Explore'><DownSquareOutlined /></Tooltip>,
      onClick: (nodeData) => {
        // 添加血缘
        // let node1 = {
        //   id: (this.state.tables.length + 1).toString(),
        //   name: `table-${this.state.tables.length + 1}`,
        //   fields: [
        //     {
        //       name: 'id',
        //       title: 'id',
        //     },
        //     {
        //       name: 'age',
        //       title: 'age'
        //     }
        //   ]
        // };
        // let node2 = {
        //   id: (this.state.tables.length + 2).toString(),
        //   name: `table-${this.state.tables.length + 2}`,
        //   fields: [
        //     {
        //       name: 'id',
        //       title: 'id',
        //     },
        //     {
        //       name: 'age',
        //       title: 'age'
        //     }
        //   ]
        // };
        // let relation1 = {
        //   srcTableId: nodeData.id,
        //   tgtTableId: node1.id,
        //   srcTableColName: 'id',
        //   tgtTableColName: 'age'
        // };
        // let relation2 = {
        //   srcTableId: node2.id,
        //   tgtTableId: nodeData.id,
        //   srcTableColName: 'id',
        //   tgtTableColName: 'age'
        // }
        // let _tables = _.cloneDeep(this.state.tables);
        // _tables.push(node1);
        // _tables.push(node2);
        // let _relations = _.cloneDeep(this.state.relations);
        // _relations.push(relation1);
        // _relations.push(relation2);
        // this.setState({
        //   tables: _tables,
        //   relations: _relations,
        //   centerId: nodeData.id
        // });
      }
    }];

    // 测试修改actionMenu
    // setTimeout(() => {
    //   let actionMenu = _.cloneDeep(this.state.actionMenu);
    //   actionMenu.push({
    //     icon: <StarOutlined />,
    //     key: 'star2',
    //     onClick: () => {
    //       alert('点击收藏2！')
    //     }
    //   });
    //   this.setState({
    //     actionMenu
    //   });
    // }, 5000)
  }
  render() {
    return (
      <LineageDag
        tables={this.state.tables}
        relations={this.state.relations}
        columns={this.columns}
        operator={this.operator}
        centerId={this.state.centerId}
        onLoaded={(canvas) => {
          this.setState({
            canvas
          });
        }}
        config={{
          titleRender: (title, node) => {
            return <div className="title-test" onClick={() => {
              let tables = _.cloneDeep(this.state.tables);
              tables.forEach((item) => {
                item.name = 'title change';
              });
              this.setState({
                tables
              }, () => {
                this.state.canvas.nodes.forEach((item) => {
                  item.redrawTitle();
                });
              });
            }}>{title}</div>
          },
          minimap: {
            enable: true
          }
        }}

        actionMenu={this.state.actionMenu}
      />
    );
  }
}


