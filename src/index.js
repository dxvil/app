import React from "react";
import App from "./component/App";
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import {Layout, Tooltip} from 'antd';

const {Header} = Layout;


ReactDOM.render((
    <Router>
      <Layout>
        <Header className='header'>DTDesign-React数据血缘图</Header>
        <Layout>
          <App />
        </Layout>
      </Layout>
    </Router>
  ), document.getElementById('root'));

